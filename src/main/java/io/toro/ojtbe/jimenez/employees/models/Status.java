package io.toro.ojtbe.jimenez.employees.models;

enum Status {
    ACTIVE, CLOSED, FROZEN;
}
