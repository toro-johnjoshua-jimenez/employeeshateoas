package io.toro.ojtbe.jimenez.employees.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
final class Product {

    @Column(columnDefinition = "varchar(10)", name = "product_cd")
    @Max(value = 10, message = "ProductCd should not exceed by 10 characters. ")
    private @Id String product;

    @NotNull
    @Column(columnDefinition = "varchar(50)")
    @Max(value = 50, message = "Name should not exceed by 50 characters.")
    private String name;

    @OneToOne
    @JoinColumn(name = "product_type_cd")
    private ProductType productType;

    @Column(columnDefinition = "date", name = "date_offered")
    @Temporal(TemporalType.DATE)
    private Date dateOffered;

    @Column(columnDefinition = "date", name = "date_retired")
    @Temporal(TemporalType.DATE)
    private Date dateRetired;
}
