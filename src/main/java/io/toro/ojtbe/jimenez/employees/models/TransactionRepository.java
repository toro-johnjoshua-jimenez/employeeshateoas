package io.toro.ojtbe.jimenez.employees.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface TransactionRepository extends JpaRepository<Transaction, Integer>, QuerydslPredicateExecutor<TransactionRepository> {
}
