package io.toro.ojtbe.jimenez.employees.models;

enum TransactionType {
    DBT, CDT;
}
