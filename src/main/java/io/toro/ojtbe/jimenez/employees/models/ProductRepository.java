package io.toro.ojtbe.jimenez.employees.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, String>, QuerydslPredicateExecutor<ProductRepository> {
}
