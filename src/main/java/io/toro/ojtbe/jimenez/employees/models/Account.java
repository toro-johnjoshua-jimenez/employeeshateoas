package io.toro.ojtbe.jimenez.employees.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
final class Account {

    @Column(columnDefinition = "serial", name = "account_id")
    private @GeneratedValue(strategy = GenerationType.IDENTITY) @Id int accountId;

    @NotNull
    @OneToOne
    @JoinColumn(name = "product_cd")
    private Product product;

    @NotNull
    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @NotNull
    @Column(columnDefinition = "date", name = "open_date")
    @Temporal(TemporalType.DATE)
    private Date openDate;

    @Column(columnDefinition = "date", name = "close_date")
    @Temporal(TemporalType.DATE)
    private Date closeDate;

    @Column(columnDefinition = "date", name = "last_activity_date")
    @Temporal(TemporalType.DATE)
    private Date lastActivityDate;

    @NotNull
    @Column(columnDefinition = "char(6)")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToOne
    @JoinColumn(name = "open_branch_id")
    private Branch branch;

    @OneToOne
    @JoinColumn(name = "open_employee_id")
    private Employee employee;

    @Column(columnDefinition = "float(10)", name = "avail_balance")
    @PositiveOrZero(message = "Balance should be greater than or equal to zero.")
    private float availBalance;

    @Column(columnDefinition = "float(10)", name = "pending_balance")
    private float pendingBalance;
}
