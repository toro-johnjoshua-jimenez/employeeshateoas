package io.toro.ojtbe.jimenez.employees.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
final class Individual implements Serializable {
    @Column(name = "customer_id")
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int individualId;

    @MapsId
    @OneToOne(mappedBy = "individual")
    @JoinColumn(name = "customer_id")
    private Customer customerId;

    @NotNull
    @Column(columnDefinition = "varchar(30)")
    @Max(value = 30, message = "FName should not exceed by 30 characters.")
    private String fName;

    @NotNull
    @Column(columnDefinition = "varchar(30)")
    @Max(value = 30, message = "LName should not exceed by 30 characters.")
    private String lName;

    @Column(name = "birth_date")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
}
