package io.toro.ojtbe.jimenez.employees.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
final class Customer implements Serializable {

    @Column(columnDefinition = "serial", name = "customer_id")
    private @GeneratedValue(strategy = GenerationType.IDENTITY) @Id int customerId;

    @NotNull
    @Max(value = 12, message = "FedId should not exceed by 12 characters.")
    @Column(columnDefinition = "varchar(12)", name = "fed_id")
    private String fedId;

    @NotNull
    @Column(columnDefinition = "char(1)", name = "cust_type_cd")
    @Enumerated(EnumType.STRING)
    private CustomerType customerType;

    @Column(columnDefinition = "varchar(30)")
    @Max(value = 30, message = "Address should not exceed by 30 characters.")
    private String address;

    @Column(columnDefinition = "varchar(20)")
    @Max(value = 20, message = "City should not exceed by 20 characters.")
    private String city;

    @Column(columnDefinition = "varchar(20)")
    @Max(value = 20, message = "State should not exceed by 20 characters.")
    private String state;

    @Column(columnDefinition = "varchar(10)", name = "postal_code")
    @Max(value = 10, message = "PostalCode should not exceed by 10 characters.")
    private String postalCode;

    @OneToOne
    @PrimaryKeyJoinColumn
    @JsonIgnore
    private Individual individual;

    @OneToOne
    @PrimaryKeyJoinColumn
    @JsonIgnore
    private Business business;
}
