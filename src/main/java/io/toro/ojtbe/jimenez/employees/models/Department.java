package io.toro.ojtbe.jimenez.employees.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Data
@Entity
@NoArgsConstructor
final class Department {
    @Column(columnDefinition = "serial", name = "department_id")
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int departmentId;

    @NotNull
    @Column(columnDefinition = "varchar(20)")
    @Max(value = 20, message = "Name should not exceed by 20 characters.")
    private String name;
}
