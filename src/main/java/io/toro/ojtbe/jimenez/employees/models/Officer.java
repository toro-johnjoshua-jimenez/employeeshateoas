package io.toro.ojtbe.jimenez.employees.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
final class Officer implements Serializable {

    @Column(columnDefinition = "serial", name = "officer_id")
    private @GeneratedValue(strategy = GenerationType.IDENTITY) @Id int officerId;

    @NotNull
    @OneToOne
    @JoinColumn(name = "customer_id")
    private Business customerId;

    @NotNull
    @Column(columnDefinition = "varchar(30)")
    @Max(value = 30, message = "FName should not exceed by 30 characters. ")
    private String fName;

    @NotNull
    @Column(columnDefinition = "varchar(30)")
    @Max(value = 30, message = "LName should not exceed by 30 characters. ")
    private String lName;

    @Column(columnDefinition = "varchar(20)")
    @Max(value = 20, message = "Title should not exceed by 20 characters. ")
    private String title;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(columnDefinition = "date", name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(columnDefinition = "date", name = "end_date")
    private Date endDate;
}
