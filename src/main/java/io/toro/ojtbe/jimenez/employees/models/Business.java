package io.toro.ojtbe.jimenez.employees.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
final class Business implements Serializable {
    @Column(name = "customer_id")
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int businessId;

    @MapsId
    @OneToOne(mappedBy = "business")
    @JoinColumn(name = "customer_id")
    @JsonBackReference
    private Customer customerId;

    @NotNull
    @Column(columnDefinition = "varchar(40)")
    @Max(value = 40, message = "Name should not exceed by 40 characters.")
    private String name;

    @NotNull
    @Column(columnDefinition = "varchar(10)", name = "state_id")
    @Max(value = 10, message = "StateId should not exceed by 10 characters.")
    private String stateId;

    @Column(name = "incorp_date")
    @Temporal(TemporalType.DATE)
    private Date incorpDate;
}
