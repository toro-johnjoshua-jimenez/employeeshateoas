package io.toro.ojtbe.jimenez.employees.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@NoArgsConstructor
final class Branch {
    @Column(columnDefinition = "serial", name = "branch_id")
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int branchId;

    @NotNull
    @Column(columnDefinition = "varchar(20)")
    @Max(value = 20, message = "Name should not exceed by 20 characters. ")
    private String name;

    @Column(columnDefinition = "varchar(30)")
    @Max(value = 30, message = "Address should not exceed by 30 characters. ")
    private String address;

    @Column(columnDefinition = "varchar(20)")
    @Max(value = 20, message = "City should not exceed by 20 characters. ")
    private String city;

    @Column(columnDefinition = "varchar(2)")
    @Size(min = 2, max = 2, message = "State should contain two letters.")
    private String state;

    @Column(columnDefinition = "varchar(12)")
    @Max(value = 12, message = "Zip code should not exceed at least 20 characters. ")
    private String zip;
}
