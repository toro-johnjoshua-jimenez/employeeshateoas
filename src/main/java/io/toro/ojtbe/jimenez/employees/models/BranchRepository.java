package io.toro.ojtbe.jimenez.employees.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface BranchRepository extends JpaRepository<Branch, Integer>, QuerydslPredicateExecutor<Branch> {
    public Branch findByName(String name);
}
