package io.toro.ojtbe.jimenez.employees.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Data
@Entity
@NoArgsConstructor
@Table(name = "product_type")
final class ProductType {
    @Column(columnDefinition = "varchar(10)", name = "product_type_cd")
    @Max(value = 10, message = "ProductType should not exceed by 10 characters. ")
    private @Id String productType;

    @NotNull
    @Column(columnDefinition = "varchar(50)")
    @Max(value = 50, message = "Name should not exceed by 50 characters. ")
    private String name;
}
