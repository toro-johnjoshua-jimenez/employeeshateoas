package io.toro.ojtbe.jimenez.employees.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
final class Transaction {
    @Column(columnDefinition = "serial", name = "txn_id")
    private @GeneratedValue(strategy = GenerationType.IDENTITY) @Id int transactionId;

    @NotNull
    @Column(columnDefinition = "timestamp", name = "txn_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;

    @NotNull
    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(columnDefinition = "char(3)", name = "txn_type_cd")
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @NotNull
    @Column(columnDefinition = "float(10)")
    private float amount;

    @OneToOne
    @JoinColumn(name = "teller_employee_id")
    private Employee tellerEmployee;

    @OneToOne
    @JoinColumn(name = "execution_branch_id")
    private Branch executionBranch;

    @Column(columnDefinition = "timestamp", name = "funds_avail_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fundsAvailDate;
}
