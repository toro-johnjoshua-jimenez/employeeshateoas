package io.toro.ojtbe.jimenez.employees.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

public interface ProductTypeRepository extends JpaRepository<ProductType, String>, QuerydslPredicateExecutor<ProductType> {
}
