package io.toro.ojtbe.jimenez.employees.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@refid")
final class Employee {
    @Column(columnDefinition = "serial", name = "employee_id")
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY)  int employeeId;

    @NotNull
    @Column(columnDefinition = "varchar(20)")
    @Max(value = 20, message = "FName should not exceed by 20 characters.")
    private String fName;

    @NotNull
    @Column(columnDefinition = "varchar(20)")
    @Max(value= 20, message = "LName should not exceed by 20 characters.")
    private String lName;

    @NotNull
    @Column(columnDefinition = "date", name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(columnDefinition = "date", name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @OneToOne
    @JoinColumn(name = "superior_employee_id")
    private Employee superiorEmployee;

    @OneToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @Column(columnDefinition = "varchar(20)")
    @Max(value = 20, message = "Title should not exceed by 20 characters. ")
    private String title;

    @OneToOne
    @JoinColumn(name = "assigned_branch_id")
    private Branch assignedBranch;
}
