package io.toro.ojtbe.jimenez.employees.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface BusinessRepository extends JpaRepository<Business, Integer>, QuerydslPredicateExecutor<Business> {
}
